INSERT INTO location (street, building, room) VALUES ('MS 2 project', 'building1', 'room1');

-- Insert into the schedule table and retrieve the generated ID
INSERT INTO schedule (owner, semester)
VALUES (1, '1st sched');
INSERT INTO schedule (owner, semester)
VALUES (2, '1st sched');
INSERT INTO schedule (owner, semester)
VALUES (3, '1st sched');

-- Use a temporary table to store the last inserted ID from the schedule table
--CREATE TABLE temp_schedule_id AS SELECT MAX(id) AS id FROM schedule;

-- Insert into the class table and retrieve the generated ID
INSERT INTO class (number, type, capacity, course)
VALUES ('Class101', 'LECTURE', 50, 1);

---- Use a temporary table to store the last inserted ID from the class table
--CREATE TABLE temp_class_id AS SELECT MAX(id) AS id FROM class;


---- Insert into the class_schedule table using the retrieved IDs from temporary tables
--INSERT INTO class_schedule (id, id_schedule, id_class)
--VALUES (1, (SELECT id FROM temp_schedule_id), (SELECT id FROM temp_class_id));

--INSERT INTO classschedule (schedule_id, class_id) VALUES (3, 1);

---- Clean up the temporary tables
--DROP TABLE temp_schedule_id;
--DROP TABLE temp_class_id;

INSERT INTO class (number, type, capacity, course)
VALUES ('Class102', 'EXERCISE', 20, 1);

-- Inserting data into the class_schedule table
INSERT INTO class_schedule (id_schedule, id_class) VALUES (1, 1);
INSERT INTO class_schedule (id_schedule, id_class) VALUES (1, 2);
