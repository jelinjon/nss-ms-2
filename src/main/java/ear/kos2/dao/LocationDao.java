package ear.kos2.dao;

import ear.kos2.model.Location;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LocationDao extends BaseDao<Location>{
    protected LocationDao() {
        super(Location.class);
    }

    public List<Location> findAllbyStreet(String street){
        List<Location>result = new ArrayList<>();
        for (Location c : this.findAll()) {
            if(c.getStreet().equals(street)){
                result.add(c);
            }
        }
        if(result.isEmpty()){
            throw new IllegalArgumentException("No locations exist on that street");
        }
        return result;
    }
}
