package ear.kos2.dao;

import ear.kos2.model.Schedule;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ScheduleDao extends BaseDao<Schedule>{
    protected ScheduleDao() {
        super(Schedule.class);
    }


    public List<Object[]> findAllSchedulesWithClassIds() {
        String jpql = "SELECT s.id, s.owner, s.semester, cs.sClass.id " +
                "FROM Schedule s " +
                "LEFT JOIN s.classSchedules cs";
        TypedQuery<Object[]> query = em.createQuery(jpql, Object[].class);
        return query.getResultList();
    }

    public Schedule findByUserId(Integer id) {
        try {
            return em.createNamedQuery("Schedule.findByUserId", Schedule.class).setParameter("userId", id)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
