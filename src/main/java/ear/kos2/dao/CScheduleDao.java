package ear.kos2.dao;

import ear.kos2.model.CSchedule;
import ear.kos2.model.SClass;
import org.springframework.stereotype.Repository;

@Repository
public class CScheduleDao extends BaseDao<CSchedule>{
    protected CScheduleDao() {
        super(CSchedule.class);
    }
}
