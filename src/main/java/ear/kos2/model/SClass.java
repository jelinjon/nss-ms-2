package ear.kos2.model;

import jakarta.persistence.*;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "class")
//@SecondaryTable(name = "class_schedule")
public class SClass extends AbstractEntity {
    @Basic
    @Column(name = "number", nullable = true, length = 20)
    private String number;
    @Basic
    @Column(name = "type", nullable = true)
    private String type;
    @Basic
    @Column(name = "capacity", nullable = true)
    private Integer capacity;
    @Basic
    @Column(name = "course", nullable = true)
    private Integer course;
    @Basic
    @Column(name = "teachers", nullable = true)
    private List<Integer> teachers;
    @ManyToOne
    @JoinColumn(name = "location")
    private Location location;

    @OneToMany(mappedBy = "sClass", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CSchedule> classSchedules = new ArrayList<>();

    public void addTeacher(Integer teacherId) {
        this.teachers.add(teacherId);
    }

    public SClass(){
    }

    public void setTime(Day day, LocalTime start, LocalTime end) {
//        this.day = day;
//        this.start = start;
//        this.end = end;
    }

    /**
     * enum for assigning lecture or exercise to a certain class <br>
     * code == 0 if LECTURE <br>
     * code == 1 if EXERCISE
     */
    private enum ClassType{
        LECTURE (0),
        EXERXCISE (1);

        private final int code;
        ClassType(int i) {
            this.code = i;
        }
    }

    @Override
    public String toString(){
        return "Class{ " + "number=" + number + ", type=" + type + ", capacity=" + capacity
                + ", course=" + course + ", location=" + location + "}";
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public List<Integer> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Integer> teachers) {
        this.teachers = teachers;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
