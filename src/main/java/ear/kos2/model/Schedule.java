package ear.kos2.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "schedule")
@NamedQueries({
        @NamedQuery(name = "Schedule.findByUserId", query = "SELECT u FROM Schedule u WHERE u.owner = :userId")
})
public class Schedule extends AbstractEntity {
    @Basic
    @Column(name = "owner", nullable = false, length = 20)
    private Integer owner;
    @Basic
    @Column(name = "semester", nullable = false, length = 20)
    private String semester;
//    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
//    @JoinTable(name = "class_schedule",
//            joinColumns = @JoinColumn(name = "id_schedule"),
//            inverseJoinColumns = @JoinColumn(name = "id_class")
//    )
//    private List<SClass> classes;

    @OneToMany(mappedBy = "schedule", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CSchedule> classSchedules = new ArrayList<>();

    public Schedule() {}

    public Schedule(Integer owner) {
        this.owner = owner;
    }

//    public void addClass(SClass c) {
//        this.classes.add(c);
//    }

//    public void removeClass(SClass c) {
//        classes.remove(c);
//    }
//
//    public List<SClass> getClasses() {
//        return this.classes;
//    }
//
//    public void add(SClass sclass) {
//        Objects.requireNonNull(sclass);
//
//        if (classes == null) {
//            classes = new ArrayList<>();
//        }
//        if (!classes.contains(sclass)) {
//            classes.add(sclass);
//        }
//    }

    @Override
    public String toString(){
        return "Schedule{ " + "owner=" + owner + ", semester=" + semester + "}";

    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

//    public void setClasses(List<SClass> classes) {
//        this.classes = classes;
//    }
}
