package ear.kos2.model;

import jakarta.persistence.*;


@Entity
@Table(name = "class_schedule")
public class CSchedule extends AbstractEntity {

//    @Basic
//    @Column(name = "schedule_id", nullable = true, length = 20)
//    private Integer schedule_id;
//    @Basic
//    @Column(name = "class_id", nullable = true, length = 20)
//    private Integer class_id;

    @ManyToOne
    @JoinColumn(name = "id_schedule", nullable = false)
    private Schedule schedule;

    @ManyToOne
    @JoinColumn(name = "id_class", nullable = false)
    private SClass sClass;

    public CSchedule() {}

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public SClass getsClass() {
        return sClass;
    }

    public void setsClass(SClass sClass) {
        this.sClass = sClass;
    }
}

