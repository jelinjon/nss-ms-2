package ear.kos2.model;

import jakarta.persistence.*;
@Entity
@Table(name = "location", uniqueConstraints = {@UniqueConstraint(columnNames = {"street", "building", "room"})})
public class Location extends AbstractEntity {
    @Basic
    @Column(name = "street", nullable = false, length = 20)
    private String street;
    @Basic
    @Column(name = "building", nullable = false, length = 20)
    private String building;
    @Basic
    @Column(name = "room", nullable = false, length = 20)
    private String room;

    public void update(String street, String building, String room) {
        if (street != null) {
            this.street = street;
        }
        if (building != null) {
            this.building = building;
        }
        if (room != null) {
            this.room = room;
        }
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public String toString(){
        return "Location{ " + "street=" + street + ", building=" + building + ", room=" + room  + "}";
    }
}
