package ear.kos2.model;

public enum Day {
    MONDAY(0),
    TUESDAY(1),
    WEDNESDAY(2),
    THURSDAY(3),
    FRIDAY(4);

    private final int code;

    Day(int code) {
        this.code = code;
    }
}
