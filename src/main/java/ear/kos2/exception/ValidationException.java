package ear.kos2.exception;

/**
 * Signifies that invalid data have been provided to the application.
 */
public class ValidationException extends GeneralException {

    public ValidationException(String message) {
        super(message);
    }
}
