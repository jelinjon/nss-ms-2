package ear.kos2.service;

import ear.kos2.dao.ScheduleDao;
import ear.kos2.model.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ScheduleService {

    private final ScheduleDao dao;

    @Autowired
    public ScheduleService(ScheduleDao dao) {
        this.dao = dao;
    }

    @Transactional(readOnly = true)
    public List<Schedule> findAll() {
        return dao.findAll();
    }

    @Transactional
    public List<Object[]> findAllSchedulesWithClasses() {
        return dao.findAllSchedulesWithClassIds();
    }


    @Transactional(readOnly = true)
    public Schedule find(Integer id) {
        return dao.find(id);
    }

    @Transactional(readOnly = true)
    public Schedule findByUserId(Integer id) {
        return dao.findByUserId(id);
    }

    @Transactional
    public void persist(Schedule course) {
        dao.persist(course);
    }

    @Transactional
    public void update(Schedule course) {
        dao.update(course);
    }

    @Transactional
    public void remove(Schedule course){
        dao.remove(course);
    }
}
