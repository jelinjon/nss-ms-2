package ear.kos2.service;

import ear.kos2.dao.LocationDao;
import ear.kos2.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LocationService {
    private final LocationDao dao;

    @Autowired
    public LocationService(LocationDao dao) {
        this.dao = dao;
    }

    @Transactional(readOnly = true)
    public List<Location> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public List<Location> findAll(String street) {
        return dao.findAllbyStreet(street);
    }

    @Transactional(readOnly = true)
    public Location find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public void persist(Location location) {
        dao.persist(location);
    }

    @Transactional
    public void update(Location location) {
        dao.update(location);
    }

    @Transactional
    public void remove(Location location){
        dao.remove(location);
    }
}
