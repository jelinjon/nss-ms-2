package ear.kos2.service;

import ear.kos2.dao.SClassDao;
import ear.kos2.model.SClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SClassService {

    private final SClassDao dao;

    @Autowired
    public SClassService(SClassDao dao) {
        this.dao = dao;
    }

    @Transactional(readOnly = true)
    public List<SClass> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public SClass find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public void persist(SClass course) {
        dao.persist(course);
    }

    @Transactional
    public void update(SClass course) {
        dao.update(course);
    }

    @Transactional
    public void remove(SClass course){
        dao.remove(course);
    }
}
