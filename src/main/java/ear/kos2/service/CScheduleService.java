package ear.kos2.service;

import ear.kos2.dao.CScheduleDao;
import ear.kos2.model.CSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CScheduleService {
    private final CScheduleDao dao;

    @Autowired
    public CScheduleService(CScheduleDao dao) {
        this.dao = dao;
    }

    @Transactional
    public void persist(CSchedule cschedule) {
        dao.persist(cschedule);
    }

    @Transactional
    public void update(CSchedule cschedule) {
        dao.update(cschedule);
    }
}
