package ear.kos2.service;

import ear.kos2.model.Role;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

@Component
public class SystemInit {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInit.class);

    private final PlatformTransactionManager txManager;

    @Autowired
    public SystemInit(PlatformTransactionManager txManager) {
        this.txManager = txManager;
    }

    @PostConstruct
    private void initSystem() {
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute((status) -> {
            return null;
        });
    }
}

