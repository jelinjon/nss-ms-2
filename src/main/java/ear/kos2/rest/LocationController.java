package ear.kos2.rest;

import ear.kos2.exception.NotFoundException;
import ear.kos2.exception.ValidationException;
import ear.kos2.model.Location;
import ear.kos2.rest.util.RestUtils;
import ear.kos2.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/location")
//@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
public class LocationController {
    private static final Logger LOG = LoggerFactory.getLogger(LocationController.class);
    private final LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Location> getLocations() {
        return locationService.findAll();
    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createLocation(@RequestBody Location location) {
        locationService.persist(location);
        LOG.debug("Created location {}.", location);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", location.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Location getLocation(@PathVariable Integer id) {
        final Location p = locationService.find(id);
        if (p == null) {
            throw NotFoundException.create("Location", id);
        }
        return p;
    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateLocation(@PathVariable Integer id, @RequestBody Location location) {
        final Location original = getLocation(id);
        if (!original.getId().equals(id)) {
            throw new ValidationException("Location identifier in the data does not match the one in the request URL.");
        }
        original.setBuilding(location.getBuilding());
        original.setRoom(location.getRoom());
        original.setStreet(location.getStreet());
        locationService.update(original);
        LOG.debug("Updated location {}.", location);
    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeLocation(@PathVariable Integer id) {
        final Location toRemove = locationService.find(id);
        if (toRemove == null) {
            return;
        }
        locationService.remove(toRemove);
        LOG.debug("Removed location {}.", toRemove);
    }
}

