package ear.kos2.rest;

import ear.kos2.exception.NotFoundException;
import ear.kos2.exception.ValidationException;
import ear.kos2.model.CSchedule;
import ear.kos2.model.SClass;
import ear.kos2.model.Schedule;
import ear.kos2.rest.util.RestUtils;
import ear.kos2.service.CScheduleService;
import ear.kos2.service.SClassService;
import ear.kos2.service.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/schedule")
//@PreAuthorize("hasAnyRole('ROLE_TEACHER', 'ROLE_STUDENT', 'ROLE_GUEST')")
public class ScheduleController {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduleController.class);
    private final ScheduleService scheduleService;
    private final SClassService sclassService;
    private final CScheduleService cScheduleService;

    @Autowired
    public ScheduleController(
            ScheduleService scheduleService,
            SClassService sclassService,
            CScheduleService cScheduleService
    ) {
        this.scheduleService = scheduleService;
        this.sclassService = sclassService;
        this.cScheduleService = cScheduleService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Schedule> getSchedules() {
        return scheduleService.findAll();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/all")
    public List<Object[]> getSchedulesWithClasses() {
        return scheduleService.findAllSchedulesWithClasses();
    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, path = "/create")
    public ResponseEntity<Void> createSchedule(@RequestBody Schedule schedule) {
        scheduleService.persist(schedule);
        LOG.debug("Created schedule {}.", schedule);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", schedule.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Schedule getSchedule(@PathVariable Integer id) {
        final Schedule p = scheduleService.find(id);
        if (p == null) {
            throw NotFoundException.create("Schedule", id);
        }
        return p;
    }
    @GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Schedule getScheduleForUserId(@PathVariable Integer id) {
        final Schedule p = scheduleService.findByUserId(id);
        if (p == null) {
            throw NotFoundException.create("Schedule", id);
        }
        return p;
    }

    @PostMapping(value = "/add_class/{classId}/student/{studentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addClassToSchedule(@PathVariable Integer classId, @PathVariable Integer studentId) {
        try {
            final Schedule tempSchedule = scheduleService.findByUserId(studentId);
            if (tempSchedule == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Schedule not found for student ID: " + studentId);
            }

            final SClass tempClass = sclassService.find(classId);
            if (tempClass == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Class not found for class ID: " + classId);
            }


            CSchedule cSchedule = new CSchedule();
            cSchedule.setSchedule(tempSchedule);
            cSchedule.setsClass(tempClass);
            cScheduleService.persist(cSchedule);


//            tempSchedule.getClasses().add(tempClass);
            scheduleService.update(tempSchedule);

            return ResponseEntity.status(HttpStatus.OK).body(tempSchedule);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred while adding class to schedule");
        }
    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSchedule(@PathVariable Integer id, @RequestBody Schedule schedule) {
        final Schedule original = getSchedule(id);
        if (!original.getId().equals(schedule.getId())) {
            throw new ValidationException("Schedule identifier in the data does not match the one in the request URL.");
        }
        scheduleService.update(schedule);
        LOG.debug("Updated schedule {}.", schedule);
    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeSchedule(@PathVariable Integer id) {
        final Schedule toRemove = scheduleService.find(id);
        if (toRemove == null) {
            return;
        }
        scheduleService.remove(toRemove);
        LOG.debug("Removed schedule {}.", toRemove);
    }
}

