package ear.kos2.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/some-endpoint")
public class TestReceiverController {

    @GetMapping("/{id}")
    public List<Object> getSomething(@PathVariable Integer id) {
        // Your logic to fetch and return the data
        return List.of("Data for id " + id);
    }
}

