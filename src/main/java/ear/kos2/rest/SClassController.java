package ear.kos2.rest;

import ear.kos2.exception.NotFoundException;
import ear.kos2.exception.ValidationException;
import ear.kos2.model.Role;
import ear.kos2.model.SClass;
import ear.kos2.rest.util.RestUtils;
import ear.kos2.service.SClassService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/class")
@PreAuthorize("permitAll()")
public class SClassController {
    private static final Logger LOG = LoggerFactory.getLogger(SClassController.class);

    private final SClassService sclassService;

    @Autowired
    public SClassController(SClassService sclassService) {
        this.sclassService = sclassService;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SClass> getClasses() {
        return sclassService.findAll();
    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createSClass(@RequestBody SClass sclass) {
        sclassService.persist(sclass);
        LOG.debug("Created sclass {}.", sclass);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", sclass.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public SClass getSClass(@PathVariable Integer id) {
        final SClass p = sclassService.find(id);
        if (p == null) {
            throw NotFoundException.create("SClass", id);
        }
        return p;
    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSClass(@PathVariable Integer id, @RequestBody SClass sclass) {
        final SClass original = getSClass(id);
        if (!original.getId().equals(sclass.getId())) {
            throw new ValidationException("SClass identifier in the data does not match the one in the request URL.");
        }
        sclassService.update(sclass);
        LOG.debug("Updated sclass {}.", sclass);
    }

    //TODO redo completly
//    @PreAuthorize("hasRole('ROLE_TEACHER')")
//    @PostMapping(value = "/{id}/addTeacher", consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Void> joinClassAsTeacher(Authentication auth, @PathVariable Integer id) {
//        try {
//
//            //get current user logic
//            assert auth.getPrincipal() instanceof UserDetails;
//            final Account user = ((UserDetails) auth.getPrincipal()).getUser();
//            //verify user is a teacher role
//            if(user.getRole() != Role.TEACHER){
//                throw new AccessDeniedException("Cannot join as a teacher if not a teacher");
//            }
//            //retrieve account
//            TeacherAccount current = teacherAccountService.find(user.getId());
//
//
//            //add teacher to a SClass logic
//            SClass temp = sclassService.find(id);
//            temp.addTeacher(current);
//            sclassService.update(temp);
//
//            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", id);
//            return new ResponseEntity<>(headers, HttpStatus.ACCEPTED);
//        } catch (Exception e) {
//            LOG.debug("Attempted to retrieve SClass with id {}.", id);
//            throw new NotFoundException("Teacher account or SClass not found");
//        }
//    }

//    @PreAuthorize("hasRole('ROLE_TEACHER')")
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeSClass(@PathVariable Integer id) {
        final SClass toRemove = sclassService.find(id);
        if (toRemove == null) {
            return;
        }
        sclassService.remove(toRemove);
        LOG.debug("Removed sclass {}.", toRemove);
    }
}

